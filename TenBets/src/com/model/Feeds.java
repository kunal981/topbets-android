package com.model;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class Feeds {
	List<Item> mItems;
	Item item = new Item();

	ArrayList<Item> listItem = new ArrayList<Item>();
	public static Feeds feeds;

	public static Feeds getInstance() {
		if (feeds == null) {
			feeds = new Feeds();
		}
		return feeds;

	}

	public ArrayList<Item> getListItem() {
		return listItem;
	}

	public List<Item> getList_Item() {
		return mItems;
	}

	public void setListItem(ArrayList<Item> listItem) {
		Log.e("List Item", "" + listItem);
		if (listItem != null)
			this.listItem = listItem;
	}

	public void set_ListItem(List<Item> item) {
		Log.e("List Item", "" + item);
		if (item != null)
			this.mItems = item;
	}

	public Item get_Item() {
		return item;
	}

	public void setListItem(Item listItem) {
		Log.e("List Item", "" + listItem);
		if (listItem != null)
			this.item = listItem;
	}

}
