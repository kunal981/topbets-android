package com.model;

public class Item {

	public String title;
	public String link;
	public String description;
	public String selectionPrice;
	public String eventCategory;
	public String eventTitle;
	public String selectionDescription;
	public String eventId;
	public String selectionId;
	public String eventTime;
	public String bet_status;

	public Item() {
		super();
	}

	public Item(String title, String link, String description,
			String selectionPrice, String eventCategory, String eventTitle,
			String selectionDescription, String eventId, String selectionId,
			String eventTime, String bet_status) {
		super();
		this.title = title;
		this.link = link;
		this.description = description;
		this.selectionPrice = selectionPrice;
		this.eventCategory = eventCategory;
		this.eventTitle = eventTitle;
		this.selectionDescription = selectionDescription;
		this.eventId = eventId;
		this.selectionId = selectionId;
		this.eventTime = eventTime;
		this.bet_status = bet_status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSelectionPrice() {
		return selectionPrice;
	}

	public void setSelectionPrice(String selectionPrice) {
		this.selectionPrice = selectionPrice;
	}

	public String getEventCategory() {
		return eventCategory;
	}

	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	public String getSelectionDescription() {
		return selectionDescription;
	}

	public void setSelectionDescription(String selectionDescription) {
		this.selectionDescription = selectionDescription;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getSelectionId() {
		return selectionId;
	}

	public void setSelectionId(String selectionId) {
		this.selectionId = selectionId;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public String getbet_status() {
		return bet_status;
	}

	public void setbet_status(String bet_status) {
		this.bet_status = bet_status;
	}

}
