package com.util;

public class StringConstant {

	public static final String KEY_TOP_TEN = "10_TOP";
	public static final int KEY_TOP_TEN_INTEGER = 1;
	public static final int KEY_UPCOMING_INTEGER = 2;
	public static final int KEY_FOOTBALL_INTEGER = 3;

	public static final String KEY_UPCOMING = "UPCOMING";
	public static final String KEY_FOOTBALL = "Football Matches";

}
