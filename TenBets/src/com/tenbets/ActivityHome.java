package com.tenbets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.customadapter.FeedsCustomItem_;
import com.localytics.android.LocalyticsAmpSession;
import com.model.Feeds;
import com.model.Item;
import com.pulllistview.PullToRefreshListView;
import com.pulllistview.PullToRefreshListView.OnRefreshListener;
import com.squareup.picasso.Picasso;
import com.urbanairship.UAirship;
import com.urbanairship.google.PlayServicesUtils;
import com.urbanairship.util.UAStringUtil;
import com.util.StringConstant;
import com.xmlparser.WSAdapter;
import com.xmlparser.XMLParser;

public class ActivityHome extends FragmentActivity implements OnClickListener {
	Button button_upcoming, button_football, button_topbets;
	ImageView imageview_settings;
	ProgressDialog pDialog;

	// http://rss.paddypower.com/rss/TopBets.xml
	private static final String URL = "http://217.199.187.192/androidapp.com/TopBets/getbets.xml";
	private static final String DEFAULT_URL = "http://217.199.187.192/androidapp.com/TopBets/getbets.php";
	HashMap<String, ArrayList<Item>> mapListBets;
	Feeds feeds;
	Typeface robot_medium, roboto_regular;
	boolean wifiConnected = true, mobileConnected = true;// Checks the network
															// connection and
															// sets the
															// wifiConnected and
	public static String ACTION_UPDATE_CHANNEL = "com.tenbets.ACTION_UPDATE_CHANNEL";

	// mobileConnected
	// variables accordingly.
	int active_tab = 0;
	boolean isServiceInProcess;

	private LocalyticsAmpSession localyticsSession;
	ImageView mImageView;

	private final static String TAG_LAUNCH_APP = "launch_app";
	private final static String TAG_TOP_BETS = "top_bets";
	private final static String TAG_FOOTBALL = "football";
	private final static String TAG_UPCOMING = "upcoming";
	private final static String TAG_MENUS = "menu_tap";

	private final static String TAG_TOP_BETSLOAD = "top_bets Load";

	private final static String TAG_FOOTBALL_LOADS = "reset game";

	private final static String TAG_UPCOMING_LOADS = "reset game";

	ArrayList<Item> listItems;
	FeedsCustomItem_ adapter;
	PullToRefreshListView listview;
	TextView textNoInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_);
		isServiceInProcess = false;
		mImageView = (ImageView) findViewById(R.id.imageView_banner);
		updateConnectedFlags();
		button_topbets = (Button) findViewById(R.id.button_topbets);
		button_football = (Button) findViewById(R.id.button_football);
		button_upcoming = (Button) findViewById(R.id.button_upcoming);
		imageview_settings = (ImageView) findViewById(R.id.imageView_setting);

		// Setup pull to refersh listview
		listview = (PullToRefreshListView) findViewById(R.id.listView1);
		listItems = new ArrayList<Item>();
		adapter = new FeedsCustomItem_(this, listItems);
		listview.setAdapter(adapter);
		listview.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				if (!isServiceInProcess)
					listItems.clear();
				new DownloadXmlTask().execute(URL);
			}
		});
		textNoInfo = (TextView) findViewById(R.id.id_no_info);

		robot_medium = Typeface
				.createFromAsset(getAssets(), "robotomedium.ttf");
		roboto_regular = Typeface.createFromAsset(getAssets(),
				"robotregular.ttf");

		active_tab = 0;
		button_topbets.setTypeface(robot_medium);
		button_football.setTypeface(robot_medium);
		button_upcoming.setTypeface(robot_medium);
		textNoInfo.setTypeface(roboto_regular);

		button_topbets.setOnClickListener(this);
		button_football.setOnClickListener(this);
		button_upcoming.setOnClickListener(this);
		button_topbets.setSelected(true);
		imageview_settings.setOnClickListener(this);
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Loading...");
		pDialog.show();
		pDialog.setCancelable(false);

		if (wifiConnected || mobileConnected)
			new DownloadXmlTask().execute(URL);
		else
			Toast.makeText(getApplicationContext(),
					"Please check Your Internet Connection", Toast.LENGTH_LONG)
					.show();

		// Instantiate the object
		localyticsSession = new LocalyticsAmpSession(
				this.getApplicationContext()); // Context used to access device
												// resources

		localyticsSession.open(); // open the session
		localyticsSession.upload(); // upload any data
		localyticsSession.tagEvent(TAG_LAUNCH_APP);
		// At this point, Localytics Initialization is done. After uploads
		// complete nothing
		// more will happen due to Localytics until the next time you call it.
	}

	@Override
	public void onStart() {
		super.onStart();

		// Handle any Google Play services errors
		if (PlayServicesUtils.isGooglePlayStoreAvailable()) {
			PlayServicesUtils.handleAnyPlayServicesError(this);
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		localyticsSession.open();
		localyticsSession.upload();
		localyticsSession.attach(this);

		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.cancelAll();

		// Register a local broadcast manager to listen for
		// ACTION_UPDATE_CHANNEL
		LocalBroadcastManager locationBroadcastManager = LocalBroadcastManager
				.getInstance(this);

		// Use local broadcast manager to receive registration events to update
		// the channel
		IntentFilter channelIdUpdateFilter;
		channelIdUpdateFilter = new IntentFilter();
		channelIdUpdateFilter.addAction(ACTION_UPDATE_CHANNEL);
		locationBroadcastManager.registerReceiver(channelIdUpdateReceiver,
				channelIdUpdateFilter);

		// Update the channel field
		updateChannelIdField();

		// String channelIdString = UAirship.shared().getPushManager()
		// .getChannelId();

	}

	private BroadcastReceiver channelIdUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			updateChannelIdField();
		}
	};

	private void updateChannelIdField() {
		String channelIdString = UAirship.shared().getPushManager()
				.getChannelId();
		channelIdString = UAStringUtil.isEmpty(channelIdString) ? ""
				: channelIdString;
		Log.i("Channel_Id", channelIdString);
		Log.e("Channel_Id:", "" + channelIdString);
		Log.e("getDeviceTagsEnabled: ", ""
				+ UAirship.shared().getPushManager().getDeviceTagsEnabled());
		Log.e("getAdmId: ", "" + UAirship.shared().getPushManager().getAdmId());
		Log.e("getGcmId: ", "" + UAirship.shared().getPushManager().getGcmId());
		Log.e("getAlias: ", "" + UAirship.shared().getPushManager().getAlias());
		Log.e("getUserNotificationsEnabled: ", ""
				+ UAirship.shared().getPushManager()
						.getUserNotificationsEnabled());
		Log.e("getNotificationActionGroup: ", ""
				+ UAirship.shared().getPushManager()
						.getNotificationActionGroup(channelIdString));
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		localyticsSession.detach();
		localyticsSession.close();
		localyticsSession.upload();

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		LocalBroadcastManager locationBroadcastManager = LocalBroadcastManager
				.getInstance(this);
		locationBroadcastManager.unregisterReceiver(channelIdUpdateReceiver);
		super.onDestroy();
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button_topbets:

			if (!button_topbets.isSelected()) {
				textNoInfo.setVisibility(View.GONE);
				active_tab = 0;
				button_football.setSelected(false);
				button_topbets.setSelected(true);
				button_upcoming.setSelected(false);
				updateListOnUi(mapListBets);
				localyticsSession.tagEvent(TAG_TOP_BETS);
			}
			break;
		case R.id.button_upcoming:
			if (!button_upcoming.isSelected()) {
				textNoInfo.setVisibility(View.GONE);
				active_tab = 1;
				button_football.setSelected(false);
				button_topbets.setSelected(false);
				button_upcoming.setSelected(true);
				updateListOnUi(mapListBets);
				localyticsSession.tagEvent(TAG_UPCOMING);
			}
			break;
		case R.id.button_football:
			if (!button_football.isSelected()) {
				textNoInfo.setVisibility(View.GONE);
				active_tab = 2;
				button_football.setSelected(true);
				button_topbets.setSelected(false);
				button_upcoming.setSelected(false);
				updateListOnUi(mapListBets);
				localyticsSession.tagEvent(TAG_FOOTBALL);
			}
			break;

		case R.id.imageView_setting:

			Intent i = new Intent(ActivityHome.this, ActivitySettings.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	// Implementation of AsyncTask used to download XML feed from
	// stackoverflow.com.
	private class DownloadXmlTask extends
			AsyncTask<String, Void, HashMap<String, ArrayList<Item>>> {

		@Override
		protected HashMap<String, ArrayList<Item>> doInBackground(
				String... urls) {
			try {
				isServiceInProcess = true;
				mapListBets = WSAdapter.loadXmlFromNetwork(urls[0]);
				return mapListBets;
				// return WSAdapter.loadXmlFromNetwork(getAssets().open(
				// "rssfeed.txt"));
			} catch (IOException e) {

			} catch (XmlPullParserException e) {
			}
			return null;
		}

		@Override
		protected void onPostExecute(HashMap<String, ArrayList<Item>> list) {
			if (pDialog.isShowing()) {
				pDialog.dismiss();

			}
			try {

				if (XMLParser.statusEnable.equals("1")) {
					Log.e("XMLParser.bannerUrl:", "" + XMLParser.bannerUrl);
					Picasso.with(getApplicationContext())
							.load(XMLParser.imagePATH).into(mImageView);

					mImageView.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							String url = XMLParser.bannerUrl;
							// Log.e("url:", ""+url);

							if (!url.startsWith("http://")
									&& !url.startsWith("https://"))
								url = "http://" + url;
							Log.e("final url:", "" + url);
							Intent browserIntent = new Intent(
									Intent.ACTION_VIEW, Uri.parse(url));
							startActivity(browserIntent);

						}
					});
				} else {
					mImageView.setBackgroundResource(R.drawable.banner);
				}
			} catch (Exception e) {

			}
			isServiceInProcess = false;
			updateListOnUi(list);
		}
	}

	public void updateListOnUi(HashMap<String, ArrayList<Item>> list) {

		if (list != null && list.size() != 0) {
			switch (active_tab) {
			case 0:
				listItems = mapListBets.get(StringConstant.KEY_TOP_TEN);
				if (listItems != null && listItems.size() != 0) {
					adapter.updatelist(listItems);
					localyticsSession.tagEvent(TAG_TOP_BETSLOAD);
				} else {
					listItems = new ArrayList<Item>();
					adapter.updatelist(listItems);
					textNoInfo.setVisibility(View.VISIBLE);
					textNoInfo.setText("No information for this event");
				}
				break;
			case 1:
				listItems = mapListBets.get(StringConstant.KEY_UPCOMING);
				if (listItems != null && listItems.size() != 0) {
					adapter.updatelist(listItems);
					localyticsSession.tagEvent(TAG_UPCOMING_LOADS);
				} else {
					listItems = new ArrayList<Item>();
					adapter.updatelist(listItems);
					textNoInfo.setVisibility(View.VISIBLE);
					textNoInfo.setText("No information for Upcomming event");
				}
				break;
			case 2:
				listItems = mapListBets.get(StringConstant.KEY_FOOTBALL);
				if (listItems != null && listItems.size() != 0) {
					adapter.updatelist(listItems);
					localyticsSession.tagEvent(TAG_FOOTBALL_LOADS);
				} else {
					listItems = new ArrayList<Item>();
					adapter.updatelist(listItems);
					textNoInfo.setVisibility(View.VISIBLE);
					textNoInfo.setText("No information for Upcomming event");
				}
				break;

			default:
				break;
			}
		} else {
			Toast.makeText(getApplicationContext(),
					"Error in Fetching the Feeds", Toast.LENGTH_LONG).show();
		}
		if (listview.isRefreshing()) {

			listview.onRefreshComplete();
		}

	}

	public void updateListOnUi_b(HashMap<String, ArrayList<Item>> list) {

		if (list == null) {
			Toast.makeText(getApplicationContext(),
					"Error in Fetching the Feeds", Toast.LENGTH_LONG).show();

		} else {

			mapListBets = list;
			switch (active_tab) {
			case 0:
				Feeds.getInstance().setListItem(
						mapListBets.get(StringConstant.KEY_TOP_TEN));
				ActivityHome.this.sendBroadcast(new Intent("refresh.topbets"));
				localyticsSession.tagEvent(TAG_TOP_BETSLOAD);
				break;
			case 1:
				Feeds.getInstance().setListItem(
						mapListBets.get(StringConstant.KEY_UPCOMING));
				ActivityHome.this.sendBroadcast(new Intent("refresh.upcoming"));
				localyticsSession.tagEvent(TAG_UPCOMING_LOADS);
				break;
			case 2:
				Feeds.getInstance().setListItem(
						mapListBets.get(StringConstant.KEY_FOOTBALL));
				ActivityHome.this.sendBroadcast(new Intent("refresh.football"));
				localyticsSession.tagEvent(TAG_FOOTBALL_LOADS);
				break;

			default:
				break;
			}

		}

	}

	// Checks the network connection and sets the wifiConnected and
	// mobileConnected
	// variables accordingly.
	private void updateConnectedFlags() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();
		if (activeInfo != null && activeInfo.isConnected()) {
			wifiConnected = activeInfo.getType() == ConnectivityManager.TYPE_WIFI;
			mobileConnected = activeInfo.getType() == ConnectivityManager.TYPE_MOBILE;
		} else {
			wifiConnected = false;
			mobileConnected = false;
		}
	}

}
