package com.tenbets;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.localytics.android.LocalyticsAmpSession;

public class ActivitySettings extends Activity {
	Typeface robot_medium, roboto_regular;
	TextView textview_title, textView_subtitle, textView_activitytitle;
	ToggleButton toggle;
	LocalyticsAmpSession settinglocalyticsSession;
	private final static String TAG_SETTING = "setting";
	private final static String TAG_TOGGLE = "toggle_change";

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activitysettings);
		textView_activitytitle = (TextView) findViewById(R.id.textView_title);
		textview_title = (TextView) findViewById(R.id.textView_label);
		textView_subtitle = (TextView) findViewById(R.id.textView2);
		toggle = (ToggleButton) findViewById(R.id.toggleButton1);
		robot_medium = Typeface
				.createFromAsset(getAssets(), "robotomedium.ttf");
		roboto_regular = Typeface.createFromAsset(getAssets(),
				"robotregular.ttf");
		textView_activitytitle.setTypeface(robot_medium);
		textview_title.setTypeface(robot_medium);
		textView_subtitle.setTypeface(roboto_regular);
		toggle.setTypeface(robot_medium);
		toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				Log.e("Herre e", "Cahneggege");
				settinglocalyticsSession.tagEvent(TAG_TOGGLE);
			}
		});

		// Instantiate the object
		settinglocalyticsSession = new LocalyticsAmpSession(
				this.getApplicationContext()); // Context used to access device
												// resources

		settinglocalyticsSession.open(); // open the session
		settinglocalyticsSession.upload(); // upload any data
		settinglocalyticsSession.tagEvent(TAG_SETTING);
	}

	public void finishActivity(View v) {

		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		settinglocalyticsSession.detach();
		settinglocalyticsSession.close();
		settinglocalyticsSession.upload();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
