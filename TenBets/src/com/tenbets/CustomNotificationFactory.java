package com.tenbets;

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.urbanairship.push.PushMessage;
import com.urbanairship.push.notifications.NotificationFactory;
import com.urbanairship.util.NotificationIDGenerator;

public class CustomNotificationFactory extends NotificationFactory {

	public CustomNotificationFactory(Context context) {
		super(context);
	}

	@Override
	public Notification createNotification(PushMessage message,
			int notificationId) {
		// Build the notification
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				getContext()).setContentTitle("Notification title")
				.setContentText(message.getAlert()).setAutoCancel(true)
				.setSmallIcon(R.drawable.ic_launcher);

		// To support interactive notification buttons extend the
		// NotificationCompat.Builder
		builder.extend(createNotificationActionsExtender(message,
				notificationId));

		return builder.build();
	}

	@Override
	public int getNextId(PushMessage pushMessage) {
		return NotificationIDGenerator.nextID();
	}
}