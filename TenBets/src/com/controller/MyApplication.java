package com.controller;

import android.app.Application;

import android.util.Log;

import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.UAirship;

public class MyApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		Log.e("", "enter");
		UAirship.takeOff(this, new UAirship.OnReadyCallback() {
			@Override
			public void onAirshipReady(UAirship airship) {

				// UAirship.shared().getPushManager().setUserNotificationsEnabled(true);
				// Perform any airship configurations here

				// Create a customized default notification factory
				// DefaultNotificationFactory defaultNotificationFactory = new
				// DefaultNotificationFactory(
				// getApplicationContext());
				// defaultNotificationFactory
				// .setSmallIconId(R.drawable.ic_notification);
				// defaultNotificationFactory.setColor(Notification.COLOR_DEFAULT);
				//
				// // Set it
				// airship.getPushManager().setNotificationFactory(
				// defaultNotificationFactory);
				// Enable Push
				// CustomNotificationFactory notificationFactory = new
				// CustomNotificationFactory(
				// getApplicationContext());
				// airship.getPushManager().setNotificationFactory(
				// notificationFactory);
				// airship.getPushManager().setPushEnabled(true);
				// airship.getPushManager().setUserNotificationsEnabled(true);
				// UAirship.shared().getPushManager().setUserNotificationsEnabled(true);

				com.tenbets.CustomNotificationFactory notificationFactory = new com.tenbets.CustomNotificationFactory(
						getApplicationContext());
				airship.getPushManager().setNotificationFactory(
						notificationFactory);
				airship.getPushManager().setPushEnabled(true);
				airship.getPushManager().setUserNotificationsEnabled(true);
			}
		});

		// AirshipConfigOptions options = new AirshipConfigOptions();
		// UAirship.takeOff(this, options);
		// UAirship.shared().getPushManager().setUserNotificationsEnabled(true);
	}
}
