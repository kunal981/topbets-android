package com.xmlparser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.model.Item;

public class FeedParser {

	private static final String ns = null;

	// We don't use namespaces

	public static List<Item> parse(InputStream in)
			throws XmlPullParserException, IOException {
		try {
			XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory
					.newInstance();
			XmlPullParser parser = xmlFactoryObject.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			// parser.nextTag();
			return readFeed(parser);
		} finally {
			in.close();
		}
	}

	private static List<Item> readFeed(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		List<Item> entries = new ArrayList<Item>();

		int event = parser.getEventType();
		// parser.require(XmlPullParser.START_TAG, ns, "channel");
		event = parser.next();
		while (event != XmlPullParser.END_DOCUMENT) {
			// if (parser.getEventType() != XmlPullParser.START_TAG) {
			// event = parser.next();
			// continue;
			// }
			String name = parser.getName();

			switch (event) {
			case XmlPullParser.START_TAG:
				break;
			case XmlPullParser.END_TAG:
				if (name.equals("item")) {
					entries.add(readItem(parser));
				}
				break;

			default:
				break;
			}
			event = parser.next();

		}
		return entries;
	}

	private static Item readItem(XmlPullParser parser)
			throws XmlPullParserException, IOException {

		String title = null;
		String link = null;
		String description = null;
		String selectionPrice = null;
		String eventCategory = null;
		String eventTitle = null;
		String selectionDescription = null;
		String eventId = null;
		String selectionId = null;

		int event = parser.getEventType();
		// parser.require(XmlPullParser.START_TAG, ns, "channel");
		event = parser.next();
		while (event != XmlPullParser.END_DOCUMENT) {

			String name = parser.getName();

			// if (name == null) {
			// event = parser.next();
			// continue;
			// }

			switch (event) {
			case XmlPullParser.START_DOCUMENT:
				break;
			case XmlPullParser.START_TAG:
				break;
			case XmlPullParser.END_TAG:
				if (name.equals("title")) {
					title = parser.getText();
				} else if (name.equals("link")) {
					link = parser.getText();
				} else if (name.equals("description")) {
					description = parser.getText();
				} else if (name.equals("selection:price")) {
					selectionPrice = parser.getText();
				} else if (name.equals("event:title")) {
					eventTitle = parser.getText();
				} else if (name.equals("event:category")) {
					eventCategory = parser.getText();
				} else if (name.equals("event:id")) {
					eventId = parser.getText();
				} else if (name.equals("selection:id")) {
					selectionId = parser.getText();
				} else if (name.equals("selection:description")) {
					selectionDescription = parser.getText();
				}
				break;
			case XmlPullParser.TEXT:

				break;

			default:
				break;
			}
			event = parser.next();

		}
		return null;

	}

	private static String readDesc(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String title = readText(parser);
		return title;
	}

	private static String readSelectionPrice(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String title = readText(parser);
		return title;
	}

	private static String readEventTitle(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String title = readText(parser);
		return title;
	}

	private static String readEventCategory(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String title = readText(parser);
		return title;
	}

	private static String readEventId(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String title = readText(parser);
		return title;
	}

	private static String readSelectionId(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String title = readText(parser);
		return title;
	}

	private static String readSelectionDesc(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String title = readText(parser);
		return title;
	}

	private static String readTitle(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String title = readText(parser);
		return title;
	}

	private static String readLink(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String title = readText(parser);
		return title;
	}

	// For the tags title and summary, extracts their text values.
	private static String readText(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}

	private static void skip(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}

}
