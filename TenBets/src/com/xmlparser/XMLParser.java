package com.xmlparser;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.util.Log;

import com.model.Item;
import com.util.StringConstant;

public class XMLParser {
	public static String bannerUrl;
	public static String imagePATH;
	public static String statusEnable;

	// constructor
	public XMLParser() {

	}

	/**
	 * Getting XML from URL making HTTP request
	 * 
	 * @param url
	 *            string
	 * */
	public String getXmlFromUrl(String url) {
		String xml = null;

		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);

			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			xml = EntityUtils.toString(httpEntity);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// return XML
		return xml;
	}

	/**
	 * Getting XML DOM element
	 * 
	 * @param XML
	 *            string
	 * */
	public static Document getDomElement(String xml) {
		Document doc = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {

			DocumentBuilder db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			doc = db.parse(is);

		} catch (ParserConfigurationException e) {
			Log.e("Error: ", e.getMessage());
			return null;
		} catch (SAXException e) {
			Log.e("Error: ", e.getMessage());
			return null;
		} catch (IOException e) {
			Log.e("Error: ", e.getMessage());
			return null;
		}

		return doc;
	}

	/**
	 * Getting node value
	 * 
	 * @param elem
	 *            element
	 */
	public static final String getElementValue(Node elem) {
		Node child;
		if (elem != null) {
			if (elem.hasChildNodes()) {
				for (child = elem.getFirstChild(); child != null; child = child
						.getNextSibling()) {
					if (child.getNodeType() == Node.TEXT_NODE) {
						return child.getNodeValue();
					}
				}
			}
		}
		return "";
	}

	/**
	 * Getting node value
	 * 
	 * @param Element
	 *            node
	 * @param key
	 *            string
	 * */
	public static String getValue(Element item, String str) {
		NodeList n = item.getElementsByTagName(str);
		return getElementValue(n.item(0));
	}

	public static HashMap<String, ArrayList<Item>> parse(String xml) {

		HashMap<String, ArrayList<Item>> mapEvents = new HashMap<String, ArrayList<Item>>();

		ArrayList<Item> listTopTen = new ArrayList<Item>();
		ArrayList<Item> listUpComing = new ArrayList<Item>();
		ArrayList<Item> listFootball = new ArrayList<Item>();
		Document doc = getDomElement(xml); // getting DOM element

		NodeList nl = doc.getElementsByTagName("item");

		for (int i = 0; i < nl.getLength(); i++) {
			Item itemElement = new Item();
			// creating new HashMap
			Element e = (Element) nl.item(i);
			// adding each child node to HashMap key => value
			itemElement.setTitle(getValue(e, "title"));
			itemElement.setLink(getValue(e, "link"));
			itemElement.setDescription(getValue(e, "description"));
			itemElement.setSelectionPrice(getValue(e, "selection_price"));
			itemElement.setEventCategory(getValue(e, "event_category"));
			itemElement.setEventTitle(getValue(e, "event_title"));
			itemElement.setSelectionDescription(getValue(e,
					"selection_description"));
			itemElement.setEventId(getValue(e, "event_id"));
			itemElement.setSelectionId(getValue(e, "event_type"));
			itemElement.setEventTime(getValue(e, "event_time"));
			// itemElement.setEv(getValue(e, "event_description"));
			itemElement.setbet_status(getValue(e, "bet_status"));
			// itemElement.setEventTime(getValue(e, "event:description"));
			if (i < 10) {
				listTopTen.add(itemElement);
			}
			if (listFootball.size() < 10
					&& getValue(e, "event_category").equals(
							StringConstant.KEY_FOOTBALL)) {
				listFootball.add(itemElement);
			}
			if (listUpComing.size() < 10
					&& isUpcomingEvent(getValue(e, "event_time"))) {
				listUpComing.add(itemElement);

			}
			if (listTopTen.size() == 10 && listFootball.size() == 10
					&& listUpComing.size() == 10) {
				break;
			}

		}
		NodeList nlist = doc.getElementsByTagName("banner");
		for (int i = 0; i < nlist.getLength(); i++) {

			Element e = (Element) nlist.item(i);
			imagePATH = getValue(e, "image");
			statusEnable = getValue(e, "status");
			bannerUrl = getValue(e, "link");
		}

		mapEvents.put(StringConstant.KEY_TOP_TEN, listTopTen);
		mapEvents.put(StringConstant.KEY_UPCOMING, listUpComing);
		mapEvents.put(StringConstant.KEY_FOOTBALL, listFootball);

		return mapEvents;
	}

	private static boolean isUpcomingEvent(String value) {
		boolean isUpcoming = false;
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		try {

			Date date = formatter.parse(value);

			if (date.getTime() > System.currentTimeMillis()) {
				isUpcoming = true;
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return isUpcoming;
	}
}
