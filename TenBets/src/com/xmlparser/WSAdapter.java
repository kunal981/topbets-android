package com.xmlparser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import com.model.Item;

public class WSAdapter {

	static XMLParser parser = new XMLParser();

	// Uploads XML from stackoverflow.com, parses it, and combines it with
	// HTML markup. Returns HTML string.
	public static HashMap<String, ArrayList<Item>> loadXmlFromNetwork(
			String urlString) throws XmlPullParserException, IOException {
		HashMap<String, ArrayList<Item>> entries = null;

		String xml = WSConnector.getXmlFromUrl(urlString);

		if (xml != null)
			entries = XMLParser.parse(xml);
		// Makes sure that the InputStream is closed after the app is
		// finished using it.

		return entries;
	}

	public static List<Item> loadXmlFromNetwork(InputStream stream)
			throws XmlPullParserException, IOException {
		List<Item> entries = new ArrayList<Item>();

		try {

			entries = FeedParser.parse(stream);
			// Makes sure that the InputStream is closed after the app is
			// finished using it.
		} finally {
			if (stream != null) {
				stream.close();
			}
		}

		return entries;
	}

}
