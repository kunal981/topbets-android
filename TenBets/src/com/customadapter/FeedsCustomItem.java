package com.customadapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.model.Feeds;
import com.model.Item;
import com.tenbets.R;

public class FeedsCustomItem extends BaseAdapter {
	// String[] string_menus;
	Integer[] arrayImages;
	String[] usernNames;
	String[] loves;
	LayoutInflater inflater;
	String[] usernmessage;
	String[] textcolor;
	Integer[] loveimage;
	Activity context;
	ArrayList<Integer> array_list = new ArrayList<Integer>();
	ArrayList<Integer> arrayListHeart = new ArrayList<Integer>();

	ProgressDialog pDialog;

	ArrayList<Item> arrayListFeed;
	Typeface robot_medium, roboto_regular;

	public FeedsCustomItem(Activity myActivity, ArrayList<Item> joyfull_array,
			int index) {
		// TODO Auto-generated constructor stub
		this.context = myActivity;
		arrayListFeed = joyfull_array;
		if (index == 1 || index == 2) {
			arrayListFeed = Feeds.getInstance().getListItem();
		}
		robot_medium = Typeface.createFromAsset(myActivity.getAssets(),
				"robotomedium.ttf");
		roboto_regular = Typeface.createFromAsset(myActivity.getAssets(),
				"robotregular.ttf");
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		// Log.e("SHOWING arrayListFeed ", ""+arrayListFeed);
		return arrayListFeed.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO arrayListFeed-generated method stub
		return arrayListFeed.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		View vi = convertView;
		if (convertView == null) {
			vi = inflater.inflate(R.layout.customlistitem, null);
			holder = new ViewHolder();
			holder.textView_title = (TextView) vi.findViewById(R.id.textView1);
			holder.textView_subtitle = (TextView) vi
					.findViewById(R.id.textView2);
			holder.textView_subtitle2 = (TextView) vi
					.findViewById(R.id.textView3);
			holder.button_check = (Button) vi.findViewById(R.id.button_bet);
			holder.button_betnow = (Button) vi.findViewById(R.id.button_betnow);
			vi.setTag(holder);
		} else {
			holder = (ViewHolder) vi.getTag();
		}

		holder.textView_title.setId(position);

		holder.textView_subtitle.setId(position);
		holder.textView_subtitle2.setId(position);
		holder.button_betnow.setId(position);
		holder.button_check.setId(position);

		final Item feed = arrayListFeed.get(position);

		// holder.textView_username.setText(string_menus[position]);

		holder.textView_title.setText(feed.getSelectionDescription());
		holder.textView_title.setTypeface(robot_medium);
		holder.textView_subtitle.setTypeface(roboto_regular);
		holder.textView_subtitle2.setTypeface(roboto_regular);
		holder.button_check.setTypeface(robot_medium);
		holder.button_betnow.setTypeface(robot_medium);

		holder.textView_subtitle.setText(feed.getSelectionId());
		holder.textView_subtitle2.setText(feed.getEventTime());

		String selectionrate = "" + feed.getSelectionPrice().replace("-", "/");
		selectionrate = selectionrate.replace(" ", "");
		holder.button_check.setText("" + selectionrate);

		Log.d("getbet_status: ", "" + feed.getbet_status());
		Log.d("feed.getSelectionDescription(): ",
				"" + feed.getSelectionDescription());
		Log.d("feed.getEventTime(): ", "" + feed.getEventTime());

		if (feed.getbet_status().equals("0")) {
			holder.button_betnow.setVisibility(View.INVISIBLE);
		} else {
			holder.button_betnow.setVisibility(View.VISIBLE);
		}

		holder.button_betnow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String urlLink = feed.getLink();
				// Toast.makeText(context, urlLink, Toast.LENGTH_LONG).show();
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse(urlLink));
				context.startActivity(browserIntent);

			}
		});
		return vi;
	}

	public void updatelist() {
		Feeds feed = Feeds.getInstance();
		this.arrayListFeed = feed.getListItem();
		notifyDataSetChanged();
	}

	public static class ViewHolder {
		public TextView textView_title, textView_subtitle, textView_subtitle2;
		public Button button_check, button_betnow;

	}

}
