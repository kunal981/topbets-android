package JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.model.Item;

public class jsonparser_response {
	JSONObject json;

	// constructor
	public jsonparser_response() {

	}

	public static HashMap<String, ArrayList<Item>> read_Feed(JSONObject json) {
		HashMap<String, ArrayList<Item>> entries = null;

		return entries;
	}

	public static ArrayList<Item> readFeed(JSONObject json)
			throws JSONException {
		Log.e("first json:", "" + json);
		ArrayList<Item> entries = new ArrayList<Item>();
		try {
			JSONArray jsonArray = json.getJSONArray("bets");
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);

				entries.add(readItem(jsonObject));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return entries;
	}

	public static Item readItem(JSONObject jsonObject) throws JSONException {
		Item item = new Item();
		// String title = null;
		// String link = null;
		// String description = null;
		// String selectionPrice = null;
		// String eventCategory = null;
		// String eventTitle = null;
		// String selectionDescription = null;
		// String eventId = null;
		// String selectionId = null;

		try {

			item.setTitle(jsonObject.getString("title"));
			// link = jsonObject.getString("link");
			item.setLink(jsonObject.getString("link"));
			item.setDescription(jsonObject.getString("description"));
			item.setSelectionPrice(jsonObject.getString("selection_price"));
			item.setEventTitle(jsonObject.getString("event_title"));
			item.setEventCategory(jsonObject.getString("event_category"));
			item.setSelectionDescription(jsonObject
					.getString("selection_desccription"));
			item.setEventId(jsonObject.getString("eventtype_id"));
			item.setSelectionId(jsonObject.getString("selection_id"));
			item.setbet_status(jsonObject.getString("bet_status"));
			Log.d("jsonObject.getString:",
					"" + jsonObject.getString("bet_status"));
			Log.d("item: ", "" + item.getbet_status());
			// description = jsonObject.getString("description");
			// selectionPrice = jsonObject.getString("selection_price");
			// eventTitle = jsonObject.getString("event_title");
			// eventCategory = jsonObject.getString("event_category");

			// selectionDescription = jsonObject
			// .getString("selection_desccription");
			// eventId = jsonObject.getString("eventtype_id");
			// selectionId = jsonObject.getString("selection_id");

			// mArrayList_username.add(mString_username);

		} catch (Exception e) {
			// TODO: handle exception
		}
		Log.e("item:", "" + item.toString());

		return item;

	}

}
